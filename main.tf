# Lookup the zone ID
data "aws_route53_zone" "myzone" {
  name = var.route53_zone
  #private_zone = true for private zone
}

# The ACM Certificate resource
resource "aws_acm_certificate" "cert" {
  domain_name       = var.hostname
  validation_method = "DNS"
}

# This allows ACM to validate the new certificate
resource "aws_route53_record" "cert_validation" {
  zone_id = data.aws_route53_zone.myzone.zone_id
  for_each = {
    for dvo in aws_acm_certificate.cert.domain_validation_options : dvo.domain_name => {
      name   = dvo.resource_record_name
      record = dvo.resource_record_value
      type   = dvo.resource_record_type
    }
  }

  name    = each.value.name
  type    = each.value.type
  records = [each.value.record]
  ttl     = 60
}

# This allows ACM to validate the new certificate
resource "aws_acm_certificate_validation" "cert" {
  for_each                = aws_route53_record.cert_validation
  certificate_arn         = aws_acm_certificate.cert.arn
  validation_record_fqdns = [each.value.fqdn]
}

# Output the ACM certifiate ARN
output "cert_arn" {
  description = "The ARN for the provisioned ACM certificate"
  value       = aws_acm_certificate.cert.arn
}
