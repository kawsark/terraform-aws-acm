variable "hostname" {
  description = "(Required) The FQDN for the Subject of ACM provisioned certificate"
}

variable "route53_zone" {
  description = "The AWS Route 53 Zone where a DNS validation record should be provisioned"
  default     = "hashidemos.io"
}

